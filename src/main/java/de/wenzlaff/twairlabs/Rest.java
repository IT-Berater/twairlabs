package de.wenzlaff.twairlabs;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Rest {

	private static final String REST_API_URL = "https://www.airlabs.co/api/v9/";

	private static final Logger LOG = LogManager.getLogger(Rest.class);

	/**
	 * https://www.airlabs.co/api/v9/airlines?api_key=xxx&country_code=DE
	 */
	public static String getAlleFlughäfen(String apiKey,
			String landesKennezeichen) {
		return REST_API_URL + "airports?api_key=" + apiKey + "&country_code="
				+ landesKennezeichen;
	}

	/**
	 * https://www.airlabs.co/api/v9/routes?api_key=xxx&arr_iata=HAJ
	 */
	public static String getAlleFlügeAnkunft(String apiKey,
			String iataFlughafen) {
		return REST_API_URL + "schedules?api_key=" + apiKey + "&arr_iata="
				+ iataFlughafen;
	}

	/**
	 * https://www.airlabs.co/api/v9/routes?api_key=xxx&dep_iata=HAJ
	 */
	public static String getAlleFlügeAbflug(String apiKey,
			String iataFlughafen) {
		return REST_API_URL + "schedules?api_key=" + apiKey + "&dep_iata="
				+ iataFlughafen;
	}

	public static HttpResponse<String> getRequest(String url)
			throws IOException, InterruptedException {

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create(url))
				.method("GET", HttpRequest.BodyPublishers.noBody()).build();

		return HttpClient.newHttpClient()
				.send(request, HttpResponse.BodyHandlers.ofString());
	}

	public static JSONArray getResponse(HttpResponse<String> response) {

		JSONObject body = new JSONObject(response.body());
		return body.getJSONArray("response");
	}
}
