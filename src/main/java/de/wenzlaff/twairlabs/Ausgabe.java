package de.wenzlaff.twairlabs;

import java.io.IOException;
import java.net.http.HttpResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Ausgabe im Logfile und auf der Konsole.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class Ausgabe {

	private static final Logger LOG = LogManager.getLogger(Ausgabe.class);

	public Ausgabe() {

	}

	/**
	 * Gibt alle Flughäfen aus.
	 * 
	 * Ein Flughafen:
	 * 
	 * {"icao_code":"EDDL","country_code":"DE","iata_code":"DUS","lng":6.77,"name":"Duesseldorf
	 * International Airport","lat":51.28}
	 * 
	 */
	public void printAlleFlughäfen(HttpResponse<String> response,
			String landesKennezeichen)
			throws IOException, InterruptedException {

		JSONArray flughäfen = Rest.getResponse(response);

		LOG.info("Alle " + flughäfen.length() + " Flughäfen in "
				+ landesKennezeichen + " ... ");
		for (Object flughafen : flughäfen) {
			JSONObject einzelnerFlughafen = (JSONObject) flughafen;
			String flugAn = "";
			flugAn = getIATAoderICAO(einzelnerFlughafen);
			flugAn += " " + einzelnerFlughafen.getString("name");
			LOG.info(flugAn);
		}
	}

	private String getIATAoderICAO(JSONObject flug) {
		String flugAn = "";
		try {
			// wenn ein Flughafen keinen IATA Code hat, gibt es eine ICAO
			// Code - https://de.wikipedia.org/wiki/IATA-Code
			flugAn = "IATA, " + flug.getString("iata_code") + ", ";
		} catch (JSONException e) {
			// https://de.wikipedia.org/wiki/ICAO-Code
			try {
				flugAn = "ICAO,  " + flug.getString("icao_code") + ", ";
			} catch (JSONException e1) {
				flugAn = "";
			}
		}
		return flugAn;
	}

	/**
	 * Print alle Ankünfte eines Flughafens in der Form: 2021-08-04 19:15 Von
	 * VIE Airline OS/ Nr. OS295 Status scheduled
	 * 
	 * Flughafen:
	 * {"dep_iata":"PMI","flight_number":"3591","airline_iata":"E2","arr_time":"2021-08-04
	 * 17:10","flight_iata":"E23591","status":"active"}
	 */
	public void printAnkünfte(HttpResponse<String> response, String flughafen) {

		LOG.info("Ankünfte von " + flughafen + "...");
		JSONArray flüge = Rest.getResponse(response);

		for (Object flug : flüge) {
			JSONObject flugAnkunft = (JSONObject) flug;

			String flugAn = flugAnkunft.getString("arr_time") + " von "
					+ flugAnkunft.getString("dep_iata") + " Airline "
					+ flugAnkunft.getString("airline_iata") + " Nr. "
					+ flugAnkunft.getString("flight_iata") + " Status "
					+ flugAnkunft.getString("status");
			LOG.info(flugAn);
		}
	}

	/**
	 * {"flight_number":"237","dep_time":"2021-08-06 00:05","airline_iata":"XQ",
	 * "flight_iata":"XQ237","arr_iata":"AYT","status":"scheduled"}
	 */
	public void printAbflüge(HttpResponse<String> response, String flughafen) {

		LOG.info("Abflüge nach " + flughafen + "...");
		JSONArray flüge = Rest.getResponse(response);

		for (Object flug : flüge) {
			JSONObject abflug = (JSONObject) flug;

			String flugAn = abflug.getString("dep_time") + " nach "
					+ abflug.getString("arr_iata") + " Airline "
					+ abflug.getString("airline_iata") + " Nr. "
					+ abflug.getString("flight_iata") + " Status "
					+ abflug.getString("status");
			LOG.info(flugAn);
		}
	}
}
