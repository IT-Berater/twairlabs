package de.wenzlaff.twairlabs;

import java.net.http.HttpResponse;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Alle Flüge von Hannover (Haj) oder andere Flughäfen wie z.B. US 2778
 * Flughäfen oder PL 102.
 * 
 * Benötigt mind. Java 11
 * 
 * https://www.airlabs.co/api/v9/schedules?api_key=API-KEY&arr_iata=HAJ
 * 
 * @author Thomas Wenzlaff
 *
 */
@Command(name = "Airport", mixinStandardHelpOptions = true, version = "Airport 1.0", description = "Fluginformationen ermitteln", showDefaultValues = true, footer = {
		"@|fg(green) Thomas Wenzlaff|@",
		"@|fg(red),bold http://www.wenzlaff.info|@" })
public class Airport implements Callable<Integer> {

	@Option(names = { "-k",
			"--apikey" }, description = "der api Key", required = true)
	private static String apiKey;

	@Option(names = { "-f",
			"--flughafen" }, description = "die IATA Bezeichnung vom Flughafen (default Hannover, HAJ, (EDDV)", defaultValue = "HAJ")
	private static String iataFlughafen;

	@Option(names = { "-alle",
			"--alleFlughäfen" }, description = "alle Flughäfen ldt. Länderkennzeichen, default DE 478 Flughäfen")
	private static boolean isAlleFlughäfen;

	@Option(names = { "-an", "--ankunft" }, description = "Ankunft der Flüge")
	private static boolean isAnkunft;

	@Option(names = { "-ab", "--abflug" }, description = "Abflug der Flüge")
	private static boolean isAbflug;

	@Option(names = { "-l",
			"--land" }, description = "das Landeskennzeichen ( default DE für Deutschland )", defaultValue = "DE")
	private static String landesKennezeichen;

	private static final Logger LOG = LogManager.getLogger(Airport.class);

	public static void main(String[] args) throws Exception {

		LOG.info("Start Airport abfrage ... ");
		int exitCode = new CommandLine(new Airport()).execute(args);
		System.exit(exitCode);
	}

	@Override
	public Integer call() throws Exception {

		HttpResponse<String> response;

		Ausgabe ausgabe = new Ausgabe();

		if (isAlleFlughäfen) {
			response = Rest.getRequest(Rest
					.getAlleFlughäfen(apiKey, landesKennezeichen));
			LOG.debug("Alle Flughäfen in " + landesKennezeichen + " : "
					+ response.body());
			ausgabe.printAlleFlughäfen(response, landesKennezeichen);
		}

		if (isAnkunft) {
			response = Rest.getRequest(Rest
					.getAlleFlügeAnkunft(apiKey, iataFlughafen));
			LOG.debug("Ankunft: " + response.body());
			ausgabe.printAnkünfte(response, iataFlughafen);
		}

		if (isAbflug) {
			response = Rest
					.getRequest(Rest.getAlleFlügeAbflug(apiKey, iataFlughafen));
			LOG.debug("Abflug: " + response.body());
			ausgabe.printAbflüge(response, iataFlughafen);
		}
		return 0;
	}
}
